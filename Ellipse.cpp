#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int a;
	int b;
	a = xc + x;
	b = yc + y;
	SDL_RenderDrawPoint(ren, a, b);
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x = a; int y = 0; int p;
	
	// Area 1

	p = a*a + 2*b*b - 2*b*a*a;
	while (x*x*(a*a + b*b) <= a*a*a*a)
	{
		int p = a*a + 2*b*b - 2*b*a*a;
		if (p > 0)
		{
			p = p + 4*b*b*x - 4*a*a*y;
			y = y - 1;
		}
		else
			p = p + 4*b*b*x + 6*b*b;
		x = x + 1;
		Draw4Points(xc, yc, x, y, ren);
	}

	// Area 2

	p = b*b + 2*a*a - 2*a*b*b;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) >= a*a*a*a)
	{
		if (p <= 0)
		{
			p = p + 4*a*a*y + 6*a*a;
		}
		else
		{
			p = p + 4*a*a*y - 4*b*b*x;
			x = x - 1;
		}
		y = y + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
	
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1

	int x, y, p;
	x = 0; y = b;
	p = b*b - a*a*b + (a*a) / 4;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) <= a*a*a*a)
	{
		if (p <= 0)
			p = p + 2*b*b*x + b*b;
		else
		{
			p = p + 2*b*b*x - 2*a*a*y - b*b;
			y = y - 1;
		}
		x = x + 1;
		Draw4Points(xc, yc, x, y, ren);
	}

	// Area 2

	p = b*b*(x + 0.5)*(x + 0.5) + a*a*(y - 1)*(y - 1) - a*a*b*b;
	Draw4Points(xc, yc, x, y, ren);
	while (y>0)
	{
		if (p > 0)
			p = p - 2 * a*a*y + a*a;
		else	
		{
			p = p + 2 * b*b*x - 2 * a*a*y + a*a;
			x = x + 1;
		}
		y = y - 1;
		Draw4Points(xc, yc, x, y, ren);
	}

}
