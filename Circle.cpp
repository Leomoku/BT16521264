#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
	SDL_RenderDrawPoint(ren, new_x, new_y);
	SDL_RenderDrawPoint(ren, xc + y, yc + x);
	SDL_RenderDrawPoint(ren, xc - y, yc + x);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc - y, yc - x);
	SDL_RenderDrawPoint(ren, xc + y, yc - x);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
    //7 points
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int a = R; int b = 0;
	int p = 3 - 2 * R;
	Draw8Points(xc, yc, a, b, ren);
	while (a > b)
	{
		if (p > 0){
			p = p + 4 * b - 4 * a + 10;
			a = a - 1;
		}
		else
			p = p + 4 * b + 6;
		
		b = b + 1;
		Draw8Points(xc, yc, a, b, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int a = R; int b = 0;
	int p = 1 - R;
	Draw8Points(xc, yc, a, b, ren);
	while (a > b)
	{
		if (p > 0) {
			p = p + 2 * b - 2 * a + 5;
			a = a - 1;
		}
		else
			p = p + 2 * b + 3;
			
		b = b + 1;
		Draw8Points(xc, yc, a, b, ren);
	}
}
